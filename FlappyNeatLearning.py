import sys
import pygame
import random

import neat
import os

import pickle

# ---------------------------------------


# init
pygame.init()
FPS = 80
WIDTH, HEIGHT = 800, 600
MARGIN = 50
screen = pygame.display.set_mode((WIDTH, HEIGHT))
clock = pygame.time.Clock()
STAT_FONT = pygame.font.SysFont("comicsans", 50)

# setting map

# background
top_background = pygame.image.load("images/topbackground.png").convert_alpha()
top_background = pygame.transform.scale(top_background, (WIDTH, 450))
top_background_y = 0
top_background_x = 0

# floor init
floor_image = pygame.image.load("images/ground.png").convert()
floor_image = pygame.transform.scale(floor_image, (WIDTH, 150))
floor_y = 450
floor_x = 0

# bird
bird_y = HEIGHT / 2
bird_x = 50
bird_surface = pygame.image.load("images/bird.png").convert()
bird_rect = bird_surface.get_rect(center=(bird_x, bird_y))
bird_mov = 0

class Bird :

    def __init__(self):

        self.centery = HEIGHT / 2
        self.centerx = 50

        self.bird_rect = bird_surface.get_rect(center=(50,  HEIGHT / 2))
        self.bird_mov = 0

    def setBirdRect(self, bird_rect):
        self.bird_rect = bird_rect

    def getBirdRect(self):
        return self.bird_rect

# pipes
pipe_y = HEIGHT / 2
pipe_x = 200
pipe_height = [400, 300, 200]
pipe_surface = pygame.image.load("images/pipes.png").convert()
pipe_surface = pygame.transform.scale(pipe_surface, (50, 400))
pipe_list = []

PIPE_SPAWNER = pygame.USEREVENT
pygame.time.set_timer(PIPE_SPAWNER, 800)

# Game variables
gravity = 0.25
isGameActive = True
SPEED = 10
HAUTEUR = 200
JUMP = 3

ECHEC = 10000
SUCESS = 100



def check_collision(pipes,ge,nets,birds):

    """
    :param pipes: les tuyaux actif dans le je
    :param ge: le genome associé
    :param nets: le noyau associé
    :param birds:
    :return:

    On vient verifier s'il y'a un impact, si oui alors on retire
    l'oiseau ainsi que ses informations associées
    """
    for x, bird in enumerate(birds) :
        for pipe in pipes:
            if bird.bird_rect.colliderect(pipe):
                # SCORE ECHEC
                ge[x].fitness -= 1

                birds.pop(x)
                nets.pop(x)
                ge.pop(x)



def create_pipe():
    """
    Créé de manière aléatoire des tuyaux
    renvoie un couple de tuyau positionné en haut de l'ecran ainsi qu'en bas
    :return:
    """
    random_pipe_height = random.choice(pipe_height)
    bottom_pipe = pipe_surface.get_rect(midtop=(WIDTH, random_pipe_height))
    top_pipe = pipe_surface.get_rect(midbottom=(WIDTH, random_pipe_height - HAUTEUR))
    return bottom_pipe, top_pipe



def move_pipes(pipes):
    """
    On vient déplacer tous les tuyaux du jeu.
    Si les tuyaux sortent de l'ecran on les retire de la liste
    :param pipes:
    :return:
    """
    for pipe_rect in pipes:
        pipe_rect.centerx -= SPEED

        if pipe_rect.centerx < 0:
            pipes.remove(pipe_rect)

    #return pipes


def updatePipes(pipes):

    """
    On vient mettre à joour la position de tous les tuyaux
    en fonction de sa hauteur, on determine si c'est un tuyau du haut ou du bas
    puis on vient inverser son image
    :param pipes:
    :return:
    """
    for pipe_rect in pipes:

        if pipe_rect.bottom >= HEIGHT:
            screen.blit(pipe_surface, pipe_rect)
        else:
            flip_pipe = pygame.transform.flip(pipe_surface, False, True)
            screen.blit(flip_pipe, pipe_rect)


def updateFloor():
    """
    On vient mettre à jour la position du sol.
    :return:
    """
    screen.blit(floor_image, (floor_x, floor_y))
    screen.blit(floor_image, (floor_x + WIDTH, floor_y))


def updateBird(birds):
    """
    Mise à jour de la position des oisieaux
    :param birds:
    :return:
    """

    for bird in birds :

        screen.blit(bird_surface, bird.getBirdRect())


def setTopBackground():
    screen.blit(top_background, (top_background_x, top_background_y))


# Init gen & Bird.
gen = 0
birds = []
def main_eval(genomes, config):

    global isGameActive, pipe_list, bird_mov, floor_x,bird_rect,gen,birds

    nets = []
    ge = []
    gen += 1
    score = 0

    pipe_list.clear()
    birds.clear()

    print(f'GENERATION :{gen}')
    print("==================")

    # On intencie la nouvelle génération et on y associe un oiseau.
    for genome_id, g in genomes:
        net = neat.nn.FeedForwardNetwork.create(g, config)
        nets.append(net)

        new_bird = Bird()
        birds.append(new_bird)

        g.fitness = 0
        ge.append(g)


    while isGameActive and len(birds) > 0:

        for event in pygame.event.get():

            # Si on quitte le jeu
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    bird_mov = 0
                    bird_mov -= JUMP
                if event.key == pygame.K_SPACE and isGameActive == False:
                    pipe_list.clear()
                    bird_rect.center = (50, HEIGHT / 2)
                    isGameActive = True
            if event.type == PIPE_SPAWNER:
                pipe_list.extend(create_pipe())
                for g in ge :
                    g.fitness +=5
                score += 1


        # setting game
        setTopBackground()

        # Game process

        #On vient déterminer à quel tuyaux les oiseaux vont s'adapter
        if len(pipe_list)> 0 :
            pipe_index = 0
            if len(birds) > 0 :
                if len(pipe_list) > 1 and birds[0].bird_rect.centerx + pipe_list[0].centerx + MARGIN :
                    pipe_index = 1
            else :

                isGameActive = False
                break

            # bird
            # Si les oiseaux sont toujours vivants on leur accorde une reward
            # output permet de determiner si les oiseaux doivent sauter.
            for x,  bird in enumerate(birds) :

                bird.bird_mov += gravity
                bird.bird_rect.centery += bird.bird_mov
                ge[x].fitness += 0.1
                output = nets[x].activate(( bird.bird_rect.centery, abs(bird.bird_rect.centery - pipe_list[pipe_index].centery),
                                           abs( bird.bird_rect.centery - pipe_list[pipe_index].bottom)))

                if output[0] > 0.5:
                    bird.bird_mov = 0
                    bird.bird_mov -= JUMP


        #si un oiseau dépasse le cadre on les supprime
        for x, bird in enumerate(birds) :
            if bird.bird_rect.centery > HEIGHT or bird.bird_rect.centery < 0 :
                birds.pop(x)
                nets.pop(x)
                ge.pop(x)
        # pipes
        move_pipes(pipe_list)
        updatePipes(pipe_list)

        #isGameActive = check_collision(pipe_list)
        check_collision(pipe_list,ge,nets, birds)


        # FLOOR UPDATING
        floor_x -= 1
        updateFloor()
        if floor_x <= -WIDTH:
            floor_x = 0

        # Update


        updateBird(birds)

        #Labels
        alive_label = STAT_FONT.render("Alive: " + str(len(birds)), 1, (10, 10, 10))
        screen.blit(alive_label, (10, 50))

        score_label = STAT_FONT.render("Score: " + str(score), 1, (10, 10, 10))
        screen.blit(score_label, (600, 50))

        pygame.display.update()
        clock.tick(FPS)


#On vient charger le fichier de configuration pour le réseau
def run(config_path):
    config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction,
                                neat.DefaultSpeciesSet, neat.DefaultStagnation, config_path)

    poppulation = neat.Population(config)

    poppulation.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    poppulation.add_reporter(stats)

    gagnant = poppulation.run(main_eval,50)


    with open("winner2.pkl", "wb") as f:
        pickle.dump(gagnant, f)
        f.close()

def replay_genome(config_path, genome_path="winner.pkl"):
    # Charge les fichiers de configuation
    config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction, neat.DefaultSpeciesSet, neat.DefaultStagnation, config_path)

    # Charge le génome gagnant
    with open(genome_path, "rb") as f:
        genome = pickle.load(f)

    # convertie en structure de donnée
    genomes = [(1, genome)]

    #Game
    main_eval(genomes, config)


if __name__ == '__main__':
    ##Un Tag Run pour le mode apprentissage
    ##Un Tag Replay genome pour le mode jeu
     local_dir = os.path.dirname(__file__)
     config_path = os.path.join(local_dir, "config-file.txt")

    #---SELECTIONNER MODE DE JEU---#
     #run(config_path)
     replay_genome(config_path)

