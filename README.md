# Projet d'apprentissage par renforcement 5AL2 ESGI 2020/2021 

## Jeu : FLAPPY BIRD

### Deux Mode de Jeu : 
    - Mode FlappyNeatLearning => Réseau de neuronne NEAT
    - Mode FlappyQLearning => Qlearning

### FlappyNeatLearning : 

    Permet sous une population de 100 oiseau de s'entrainer afin de
    trouver le meileur moyen de tenir le plus longtemps.
    Les birds prendront en parametre uniquement la hauteur
    des prochains tuyaux.
    
### FlappyQlearning : 

    Permet à un oiseau de s'entrainer en prenant en compte la hauteur, sa position
    et la position du prochain tayau.
    A la fin de l'entraintement, affiche un graphique avec différente courbe,
    reflétant l'effiances de l'entrainement sous un certain nombre d'epoch.
    
    - REWARD TUYAU PASSER : 1000
    - REWARD MORT : -500
    - REWARD BONNE POSITION : 2
    - REWARD MAUVAISE POSITION : -1
    
Auteur : ALLOU John, GHALEM Marc
