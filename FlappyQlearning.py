import sys
import pygame
import random
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import plotting
"""
Version Instable, Apprentissage staticielle puis génération de graphique.
Modifier la variable le GOAL.

=> Peut passer 3 Tuyaux sur une courte durée d'apprentissage.
"""
matplotlib.style.use('ggplot')


EPISODES = 100
STATS_EVERY = 5

ep_rewards = []
aggr_ep_rewards = {'ep': [], 'avg': [], 'max': [], 'min': []}

GOAL = 2000
REWARD_PASS = 1000
REWARD_MID = 1
REWARD_DEFAULT = -1
REWARD_DIE = -50
REWARD_NULL = 0

A_JUMP = 'JUMP'
A_STAND = 'STAND'

ACTIONS = [A_JUMP, A_STAND]


class Environnement:

    def __init__(self, bird_rect, pipe_list):

        self.bird_rect = bird_rect

        self.pipe_list = pipe_list

        self.next_pipe = pipe_list[0]

        self.distance_x = self.next_pipe.centerx - self.bird_rect.centerx

        self.reward_goal = REWARD_DIE

        self.starting_point = {
            "id": 0,

            "bird_rect": self.bird_rect,

            "next_pipe": self.next_pipe,

            "distance_x": self.next_pipe.centerx - self.bird_rect.centerx,
        }


        self.states = {}
        new_distance = 0
        for i in range(self.distance_x):

            new_state = {

                "id": i,

                "bird_rect": self.bird_rect,

                "next_pipe": self.next_pipe,

                "distance_x": self.next_pipe.centerx - new_distance - self.bird_rect.centerx,
            }
            new_distance += 10
            # self.next_pipe.centerx = self.next_pipe.centerx - new_distance

            self.states[i] = new_state
            if new_distance > self.distance_x + 100:
                break

    # Function

    def apply(self, state, action):




        new_state = None
        #On Fait sauter le bird
        if action == 'JUMP':
            new_state = state
            new_state['bird_rect'].centery -= 10

            new_state['next_pipe'].centerx -= 10

            new_state['distance_x'] = new_state['next_pipe'].centerx - new_state['bird_rect'].centerx

            new_state['id'] += 1

        #On fait tomber le bird
        elif action == 'STAND':
            new_state = state
            new_state['bird_rect'].centery += 1

            new_state['next_pipe'].centerx -= 10

            new_state['distance_x'] = new_state['next_pipe'].centerx - new_state['bird_rect'].centerx

            new_state['id'] += 1

        #On génère un nouveau tunnel
        if new_state['next_pipe'].centerx < WIDTH/2:
            self.pipe_list.extend(create_pipe())
            pass

        #On remplace le tuyau
        if new_state['next_pipe'].centerx < MARGIN :

            self.pipe_list.remove(self.pipe_list[1])
            self.pipe_list.remove(self.pipe_list[0])

            #self.pipe_list.append(create_pipe())
            self.next_pipe = self.pipe_list[0]
            new_state['next_pipe'] = self.next_pipe
            new_state['id'] = 0


        if new_state['id'] in self.states.keys():

            print(new_state)
            if new_state['bird_rect'].colliderect(new_state['next_pipe']):
                reward = REWARD_DIE
                print("IMPACT")

                self.bird_rect.centerx = 50
                self.bird_rect.centery = HEIGHT / 2

                self.pipe_list.remove(self.pipe_list[1])
                self.pipe_list.remove(self.pipe_list[0])

                # self.pipe_list.extend(create_pipe())
                self.next_pipe = self.pipe_list[0]
                new_state['next_pipe'] = self.next_pipe
                new_state['id'] = 0

                new_state = {

                    "id": 0,

                    "bird_rect": self.bird_rect,

                    "next_pipe": self.next_pipe,

                    "distance_x": self.next_pipe.centerx - self.bird_rect.centerx,
                }



            elif new_state['bird_rect'].centery > HEIGHT or new_state['bird_rect'].centery < 0 :
                reward = REWARD_DIE
                print("DIE")
                self.bird_rect.centerx = 50
                self.bird_rect.centery = HEIGHT / 2

                self.pipe_list.remove(self.pipe_list[1])
                self.pipe_list.remove(self.pipe_list[0])

                self.next_pipe = self.pipe_list[0]
                new_state['next_pipe'] = self.next_pipe
                new_state['id'] = 0

                new_state = {

                    "id": 0,

                    "bird_rect": self.bird_rect,

                    "next_pipe": self.next_pipe,

                    "distance_x": self.next_pipe.centerx - self.bird_rect.centerx,
                }



            elif new_state['bird_rect'].centerx + 10 > new_state['next_pipe'].centerx\
                    and new_state['next_pipe'].centery > new_state['bird_rect'].centery \
                    and new_state['next_pipe'].centery - 200 > new_state['bird_rect'].centery:

                reward = REWARD_PASS
                print("PASS")

            elif 200 <= new_state['bird_rect'].centery <= 400:
                reward = REWARD_MID

                #print("MID")

            else:
                reward = REWARD_DEFAULT
                #reward = REWARD_NULL


            return new_state, reward


    def updateAllStates(self):
        self.states = {}
        new_distance = 0
        for i in range(self.distance_x):

            new_state = {

                "id": i,

                "bird_rect": self.bird_rect,

                "next_pipe": self.next_pipe,

                "distance_x": self.next_pipe.centerx - new_distance - self.bird_rect.centerx,
            }
            new_distance += 10
            # self.next_pipe.centerx = self.next_pipe.centerx - new_distance

            self.states[i] = new_state
            if new_distance > self.distance_x + 100:
                break

    def getStates(self):
        return self.states


class Agent:
    def __init__(self, environment,bird_rect,pipe_list):
        self.environment = environment
        self.policy = Policy(environment.states.keys(), ACTIONS)
        self.bird_rect = bird_rect
        self.pipe_list = pipe_list
        self.reset()

    def reset(self):
        self.bird_rect.centerx = 50
        self.bird_rect.centery = HEIGHT/2


        self.pipe_list.remove(pipe_list[1])
        self.pipe_list.remove(pipe_list[0])

        self.pipe_list.extend(create_pipe())
        pipe_reset = self.pipe_list[0]

        self.state = {

                "id": 0,

                "bird_rect": self.bird_rect,

                "next_pipe": pipe_reset,

                "distance_x": pipe_reset.centerx - self.bird_rect.centerx,
            }

        self.previous_state = self.state
        self.score = 0

        self.environment.updateAllStates()


    def update_policy(self):
        self.policy.update(agent.previous_state, agent.state, self.last_action, self.reward)

    def best_action(self):
        return self.policy.best_action(self.state)

    def do(self, action):
        self.previous_state = self.state
        self.state, self.reward   = self.environment.apply(self.state, action)
        self.score += self.reward
        self.last_action = action


class Policy:
    def __init__(self, states, actions, learning_rate=0.1, discount_factor=0.95):
        self.table = {}
        self.learning_rate = learning_rate
        self.discount_factor = discount_factor

        for s in states:
            self.table[s] = {}
            for a in actions:
                self.table[s][a] = 0

    def best_action(self, state):
        action = None

        for a in self.table[state['id']]:
            if action is None or self.table[state['id']][a] > self.table[state['id']][action]:
                action = a
        return action


    def update(self, previous_state, state, last_action, reward):
        # Q(st, at) = Q(st, at) + learning_rate * (reward + discount_factor * max(Q(state)) - Q(st, at))


        maxQ = max(self.table[state['id']].values())
        self.table[previous_state['id']][last_action] += self.learning_rate * \
                                                         (reward + self.discount_factor * maxQ -
                                                          self.table[previous_state['id']][
                                                              last_action])

# ---------------------------------------


# init
pygame.init()
FPS = 30
WIDTH, HEIGHT = 800, 600
MARGIN = 50
screen = pygame.display.set_mode((WIDTH, HEIGHT))
clock = pygame.time.Clock()

# setting map

# background
top_background = pygame.image.load("images/topbackground.png").convert_alpha()
top_background = pygame.transform.scale(top_background, (WIDTH, 450))
top_background_y = 0
top_background_x = 0

# floor init
floor_image = pygame.image.load("images/ground.png").convert()
floor_image = pygame.transform.scale(floor_image, (WIDTH, 150))
floor_y = 450
floor_x = 0

# bird
bird_y = HEIGHT / 2
bird_x = 50
bird_surface = pygame.image.load("images/bird.png").convert()
bird_rect = bird_surface.get_rect(center=(bird_x, bird_y))
bird_mov = 0

# pipes
pipe_y = HEIGHT / 2
pipe_x = 200
pipe_height = [400, 300, 200]
pipe_surface = pygame.image.load("images/pipes.png").convert()
pipe_surface = pygame.transform.scale(pipe_surface, (50, 400))
pipe_list = []

PIPE_SPAWNER = pygame.USEREVENT
pygame.time.set_timer(PIPE_SPAWNER, 1500)

# DROP_BIRD = pygame.USEREVENT+1
# pygame.time.set_timer(DROP_BIRD, 125)


# Game variables
gravity = 0.50
isGameActive = True
SPEED = 10
HAUTEUR = 200
JUMP = 10

ECHEC = 10000
SUCESS = 100


def check_collision(pipes):
    for pipe in pipes:
        if bird_rect.colliderect(pipe):
            # SCORE ECHEC
            return False
    return True


def create_pipe():
    random_pipe_height = random.choice(pipe_height)
    bottom_pipe = pipe_surface.get_rect(midtop=(WIDTH, random_pipe_height))
    top_pipe = pipe_surface.get_rect(midbottom=(WIDTH, random_pipe_height - HAUTEUR))
    return bottom_pipe, top_pipe


pipe_list.extend(create_pipe())


def move_pipes(pipes):
    for pipe_rect in pipes:
        pipe_rect.centerx -= SPEED

        if pipe_rect.centerx < 0:
            pipes.remove(pipe_rect)

    return pipes


def updatePipes(pipes):
    for pipe_rect in pipes:

        if pipe_rect.bottom >= HEIGHT:
            screen.blit(pipe_surface, pipe_rect)
        else:
            flip_pipe = pygame.transform.flip(pipe_surface, False, True)
            screen.blit(flip_pipe, pipe_rect)


def updateFloor():
    screen.blit(floor_image, (floor_x, floor_y))
    screen.blit(floor_image, (floor_x + WIDTH, floor_y))


def updateBird():
    screen.blit(bird_surface, bird_rect)


def setTopBackground():
    screen.blit(top_background, (top_background_x, top_background_y))


def main():
    global isGameActive, pipe_list, bird_mov, floor_x
    environment = Environnement(bird_rect, pipe_list)
    # agent = Agent(environment,bird_rect)

    while True:

        # Get event in game
        for event in pygame.event.get():

            # Si on quitte le jeu
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    bird_mov = 0
                    bird_mov -= JUMP
                if event.key == pygame.K_SPACE and isGameActive == False:
                    pipe_list.clear()
                    bird_rect.center = (50, HEIGHT / 2)
                    isGameActive = True
            if event.type == PIPE_SPAWNER:
                pipe_list.extend(create_pipe())

        # setting game
        setTopBackground()

        # Game process

        if isGameActive is True:
            # bird
            bird_mov += gravity
            bird_rect.centery += bird_mov

            updateBird()

            # pipes
            pipe_list = move_pipes(pipe_list)
            updatePipes(pipe_list)

            isGameActive = check_collision(pipe_list)

        # FLOOR UPDATING
        floor_x -= 1
        updateFloor()
        if floor_x <= -WIDTH:
            floor_x = 0

        # Update
        pygame.display.update()
        clock.tick(FPS)


def mainTrain():
    pass
    # print(environment.getStates())


if __name__ == '__main__':
    # main()
    # mainTrain()
    #global isGameActive, pipe_list, bird_mov, floor_x
    environment = Environnement(bird_rect, pipe_list)
    agent = Agent(environment, bird_rect, pipe_list)


    for episode in range(EPISODES):
        agent.reset()
        print(f"EPISODE : {episode}")
        while agent.score < GOAL:
            action = agent.best_action()

            agent.do(action)
            agent.update_policy()
            ep_rewards.append(agent.score)
            print("Mon score : " + str(agent.score))



        if not episode % STATS_EVERY:
            average_reward = sum(ep_rewards[-STATS_EVERY:]) / STATS_EVERY
            aggr_ep_rewards['ep'].append(episode)
            aggr_ep_rewards['avg'].append(average_reward)
            aggr_ep_rewards['max'].append(max(ep_rewards[-STATS_EVERY:]))
            aggr_ep_rewards['min'].append(min(ep_rewards[-STATS_EVERY:]))

    plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['avg'], label="average rewards")
    plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['max'], label="max rewards")
    plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['min'], label="min rewards")
    plt.legend(loc=4)
    plt.show()
